<?php
show_admin_bar( false );

register_nav_menus( array(
	'navigation_menu' => 'Header Navigation Menu',		
	'navigation_mobile_menu' => 'Header Navigation Mobile Menu',		
	'reports_menu' => 'Reports Menu',	
	'footer_menu' => 'Footer Menu',
) );


function post_thumbnail() {
 //Featured Image
 //add_theme_support('post-thumbnails');
 //add_image_size( 'full-width-crop', 241, 153, true ); 
}
add_action('after_setup_theme', 'post_thumbnail');

add_theme_support( 'post-thumbnails' ); 

function widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Search', 'equitystory' ),
		'id'            => 'search',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<span class="hidden">',
		'after_title'   => '</span>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer', 'equitystory' ),
		'id'            => 'footer',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h2 class="front-tag">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'widgets_init' );


//Logout Redirect
add_filter('allowed_redirect_hosts','allow_ms_parent_redirect');
function allow_ms_parent_redirect($allowed)
{
    $allowed[] = 'multisiteparent.com';
    return $allowed;
}

//Login fail redirect
add_action( 'wp_login_failed', 'my_front_end_login_fail' );  // hook failed login

function my_front_end_login_fail( $username ) {
   $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
   // if there's a valid referrer, and it's not the default log-in screen
   if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
      wp_redirect( $referrer . '?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
      exit;
   }
}


// Remove Private in title text
function the_title_trim($title)
{
  $pattern[0] = '/Protected:/';
  $pattern[1] = '/Private:/';
  $replacement[0] = ''; // Enter some text to put in place of Protected:
  $replacement[1] = ''; // Enter some text to put in place of Private:

  return preg_replace($pattern, $replacement, $title);
}
add_filter('the_title', 'the_title_trim');


//Gravity form phone validation
// add_filter( 'gform_field_validation', 'validate_phone', 10, 4 );
// function validate_phone( $result, $value, $form, $field ) {
//     $pattern = "/^(\+44\s?7\d{3}|\(?07\d{3}\)|\(?01\d{3}\)?)\s?\d{3}\s?\d{3}$/";
//     if ( $field->type == 'phone' && $this->phoneFormat != 'standard' && ! preg_match( $pattern, $value ) ) {
//         $result['is_valid'] = false;
//         $result['message']  = 'Please enter a valid phone number';
//     }

//     return $result;
// }
//Gravity form hook for SSL Certificate.
add_filter( 'gform_is_ssl', '__return_true' );

//add_filter('gform_enable_credit_card_field', '__return_true');

//require get_template_directory() . '/include/cpt-slider.php';
//require get_template_directory() . '/include/cpt-page-columns.php';

require get_template_directory() . '/include/acf-options-cpt.php';


