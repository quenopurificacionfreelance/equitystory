<?php get_header(); ?>
	<div class="container">
		<div class="content">
			<?php
			wp_reset_query();
			if(have_posts()) : 
				while ( have_posts() ) : the_post();
					$cc = get_the_content();
					if($cc != '') : ?>
						<div class="info">
							<div class="infoText" style="clear:both;">
								<h1><?php the_title(); ?></h1>
								<?php the_content(); ?>
							</div>
						</div>
					<?php
					endif;
				endwhile;
			endif;
			?>
			
		
		<?php //gravity_form( 1, false, false, false, '', false ); ?>
		<?php		
			if( have_rows('module') ):				 	
				?>
				<div class="info-home"><div class="story_leftcol"> <?php
			    while ( have_rows('module') ) : the_row();
			    	wp_reset_query();
					if( get_row_layout() == 'reports' ):					
						include 'include/content-single.php';
					endif;				
				endwhile; ?>
				</div></div> <?php
			endif;
		?>		
		</div>
	</div>
<?php get_footer(); ?>