<!doctype html>
<html>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title><?php wp_title( '|', true, 'right' ); ?>Equity Story</title>

<link href="<?php bloginfo('template_directory'); ?>/css/layout.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" type="text/css" />

<!--[if IE 8]>
	<link type="text/css" rel="stylesheet" media="screen" href="<?php bloginfo('template_directory'); ?>/css/ie8.css" >
<![endif]-->

<script src="<?php bloginfo('template_directory'); ?>/js/jquery.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/scripts.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/scroll.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/AC_RunActiveContent.js" type="text/javascript"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjX7c0OjwLWJbKPzJ4Eq6hTxDL1AU67s4"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/contact-map.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/classie.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/custom.js" type="text/javascript"></script>
<script async="" src="//www.google-analytics.com/analytics.js"></script>
<!-- End Visual Website Optimizer Asynchronous Code -->
	<!--[if IE]>
	<script type="text/javascript">
	$(function(){
		$('#f a img').hover(
			function(){
				$(this).attr('src', $(this).attr('src').replace('.png', '_transparent.png'));
			},
			function(){
				$(this).attr('src', $(this).attr('src').replace('_transparent', ''));
			}
		);
	
		$('.daily_tips .bottom a img').hover(
			function(){
				$(this).attr('src', $(this).attr('src').replace('.png', '_transparent.png'));
			},
			function(){
				$(this).attr('src', $(this).attr('src').replace('_transparent', ''));
			}
		);
	});
	</script>
	<![endif]-->
	
	<!--[if lt IE 8]>
		<link  href="css/main-ie.css" rel="stylesheet" type="text/css" />	
	<![endif]-->

<?php wp_head(); ?>
</head>
<body <?php body_class( $class ); ?>>
	<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">   
        <button id="closebtn"></button>     
        <?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'container_class' => 'menu-header', 'theme_location' => 'navigation_menu', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
    </nav>
	<div class="wrapper">
	<?php //wp_register(); ?>
	<?php //wp_loginout(); ?>
		<div class="container">

			<div class="banner">
    			<div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>">
    				<img src="<?php the_field('logo', 'option'); ?>" alt="EQUITY STORY" border="0"></a>
    				<div class="clearfix"></div>
    			</div>
    			<div class="mobile">
                    <div class="main">
                        <section>
                            <button id="showLeft"><span></span>
							  <span></span>
							  <span></span>
							  <span></span>
				  			</button>
                        </section>
                    </div>
                </div>
    			<div class="mobile-menu">
    				
    				<?php
    				wp_nav_menu( array( 'sort_column' => 'menu_order', 'container_class' => 'menu-header', 'theme_location' => 'navigation_mobile_menu', 'link_before' => '<span>', 'link_after' => '</span>' ) );
    				?>
    			</div>

				<div class="client_login">
					<div class="p12 white">

					 <?php 
					 	if (is_user_logged_in()) : 
					 		$user_info = get_userdata(1);
					 		$adminurl = admin_url();
							echo '<div class="head-pos"><span class="mob">Welcome: <strong>'. $user_info->first_name . '</strong> | <a href="'.get_edit_user_link().'">My Account</a> | </span><a href="'.$adminurl.'">Administration</a> | <a href="'.wp_logout_url(esc_url( home_url( '/' ) )).'">Logout</a></div>';

					 	else :
					 		echo "<div class='desktop'>";
					 		echo "<span class='p12 white'>MEMBERS</span>";							
					 		wp_login_form( $args );
					 		echo "</div>";
					 		echo "<div class='mobile'>";
					 		
					 		$sign = get_field('pop_up_text', 'option'); 
					 		$signclass = get_field('pop_up_button', 'option'); 
					 		$login = get_field('login_text', 'option'); 
					 		$loginclass = get_field('login_button', 'option'); 
					 		echo "<a href='#' class='".$signclass."'>".$sign." </a>  / ";
					 		//echo " <a href='#' class='".$loginclass."'> ".$login." </a>";
					 		echo " <a href='/login'> ".$login." </a>";
					 		echo "</div>";	
					 		if ( 'failed' == $_GET["login"] ) :
	  							echo "<p style='color: #ff0000; clear:both;'>Invalid Username or Password. </br>Please try again.</p>";	  							
							endif;
					 	endif;
					 ?> 
						
					</div> <!-- end p21 white -->
				</div> <!-- end client_login -->
				<div class="banner_text">
					<?php the_field('banner_text', 'option'); ?>
				</div>
				<div class="clearfix"></div>
			</div>
			<div id="buttons">	
											
				<?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'container_class' => 'menu-header', 'theme_location' => 'navigation_menu', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
			</div>
			
			