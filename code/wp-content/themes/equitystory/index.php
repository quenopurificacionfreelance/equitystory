<?php get_header(); ?>
	<div class="container">
		<div class="content">
			<?php
			if ( have_posts() ) : 
				while ( have_posts() ) : the_post();
				$cc = get_the_content();
				if($cc != '') : ?>
					<div class="info">
						<div class="infoText" style="clear:both;">
							<h1><?php the_title(); ?></h1>
							<?php the_content(); ?>
						</div>
					</div>
				<?php
			endif;endwhile;endif;
			?>
			
		
		<?php //gravity_form( 1, false, false, false, '', false ); ?>
		<?php		
		if( have_rows('flexible_content') ):				 	
			?>
			<div class="info-home"> <?php
		    while ( have_rows('flexible_content') ) : the_row();
		    	wp_reset_query();
				if( get_row_layout() == 'home' ):					
					include 'include/content-home.php';
				endif;				
				
				wp_reset_query();
				if( get_row_layout() == 'module' ):
					if( have_rows('flex_module') ):
						while ( have_rows('flex_module') ) : the_row();

							wp_reset_query();
							if( get_row_layout() == 'featured_reports' ):
								include 'include/content-featured-reports.php';
							endif;

							wp_reset_query();
							if( get_row_layout() == 'full_text' ): 					
								include 'include/content-fulltext.php';
							endif;

							wp_reset_query();
							if( get_row_layout() == 'column_table' ): 					
								include 'include/content-column.php';
							endif;

							wp_reset_query();
							if( get_row_layout() == 'private_content' ): 					
								include 'include/content-private-content.php';
							endif;
							wp_reset_query();

							wp_reset_postdata();
							if( get_row_layout() == 'stocks_results' ): 					
								include 'include/content-stocks-results.php';
							endif;

							wp_reset_postdata();
							if( get_row_layout() == '2_flexible_column' ): 					
								include 'include/content-flexible-column.php';
							endif;

							//wp_reset_query();
						endwhile;
					endif;
				endif;
			endwhile; ?>
			</div> <?php
		endif;
		?>		
		</div>
	</div>
<?php get_footer(); ?>