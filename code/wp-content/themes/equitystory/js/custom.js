$(window).on("load resize scroll",function(){
    var $winwid = $(window).width();
    $('.client_login').fadeIn();
    // if($winwid >= 550) {
    //   $('.banner_text').fadeIn();  
    // }
    // else {
    //   $('.banner_text').fadeOut(); 
    // }
    

    $('.tabs .tab-links a').on('click', function(e){
        var currentAttrValue = $(this).attr('href');         
        $('.tabs ' + currentAttrValue).show().siblings().hide();
        $(this).parent('li').addClass('active').siblings().removeClass('active'); 
        e.preventDefault();
    });
//set the starting bigestHeight variable  
    var biggestHeight = 0;  
    //check each of them  
    if($('.border-r-b').length) {
	    $('.border-r-b .box').each(function(){    
	        if($(this).height() > biggestHeight){  
	            biggestHeight = $(this).height();  
	        }  
	    });      
	    $('.border-r-b .box').height(biggestHeight);
	}

	   $(".gform_wrapper input[type=text],.gform_wrapper input[type=password], .gform_wrapper select, .gform_wrapper textarea, .gform_wrapper select, #loginform input[type=text], #loginform input[type=password]").each(function(index, elem) {
      var eId = $(elem).attr("id");
      var label = null;
      if (eId && (label = $(elem).parents("form").find("label[for="+eId+"]")).length == 1) {
    $('.gfield_required').remove();
          $(elem).attr("placeholder", $(label).html());
          $(label).remove();
      }
    });

  if($winwid <= 999) {
    $('#featured-reports-content .sections .reports .featured-column .top').css('height','auto');
    var biggestHeight = 0;  
    //check each of them  
    $('#featured-reports-content .sections .reports .featured-column .top').each(function(){    
        if($(this).height() > biggestHeight){  
            biggestHeight = $(this).height();  
        }  
    });      
    $('#featured-reports-content .sections .reports .featured-column .top').height(biggestHeight);
  }
  else {
    $('#featured-reports-content .sections .reports .featured-column .top').height(90);
  }  
});

  

  $(document).ready(function() {
    var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
    showLeft = document.getElementById( 'showLeft' );

    showLeft.onclick = function() {
      classie.toggle( this, 'active' );
      classie.toggle( menuLeft, 'cbp-spmenu-open' );    
      disableOther( 'showLeft' );
    };
    $('#closebtn').click(function(){
      $('#showLeft').removeClass('active');
      $('.cbp-spmenu ').removeClass('cbp-spmenu-open');
    })

    $('#showLeft').click(function(){
      $(this).toggleClass('open');
    });
  });
