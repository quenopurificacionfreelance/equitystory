function showObj(object) {
    document.getElementById(object).style.visibility = 'visible';
    document.getElementById(object).style.display = 'block';
}
function hideObj(object) {
    document.getElementById(object).style.visibility = 'hidden';
    document.getElementById(object).style.display = 'none';
}
