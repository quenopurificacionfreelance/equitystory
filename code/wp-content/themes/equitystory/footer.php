			<div class="bottom">
				<div class="padding-mobile">
					<div class="bottom_left">
						<p>© Equity Story 2016</p>
						<p>Equity Story is an Australian owned and operated research firm producing ASX stock reports for Australian investors. If you have any questions about our service, please contact our team.</p>
						<p>Equity Story Pty Ltd (ABN 94 127 714 998) is registered with the Australian Securities and Investment Commission under Authorised AFSL 343937 </p>
						<p>Office: Building 34, Office A, Sydney Harbour Trust, Mosman 2088 (By appointment only)</p>
					</div>

					<div class="bottom_right">
						<p>
							<a href="https://www.equitystory.com.au/login">Login</a> |					
						<a href="https://www.equitystory.com.au/contact-us">CONTACT US</a> | <a href="https://www.equitystory.com.au/terms-and-conditions">FINANCIAL SERVICES GUIDE</a> | <a href="https://www.equitystory.com.au/compliance">COMPLIANCE</a></p>
				
						<div id="eWAYBlock">
						    <div style="text-align:left;">
				        		<a href="http://www.eway.com.au/secure-site-seal?i=10&amp;s=7&amp;pid=102d3f75-ce1d-4666-97ba-70e332389142" title="eWAY Payment Gateway" target="_blank" rel="nofollow">
				            	<img style="width:110px" alt="eWAY Payment Gateway" src="https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=10&amp;size=7&amp;pid=102d3f75-ce1d-4666-97ba-70e332389142"></a>
				          	</div>
						</div>
		<!-- End eWAY Linking Code -->		
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
<?php wp_footer(); ?>	
</body>
</html>