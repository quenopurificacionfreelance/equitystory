<?php
	$navmenu = get_sub_field('choose_menu');
	$privatebg = get_sub_field('background_image');	
	$choose_content = get_sub_field('choose_content');
	$tt1 = get_sub_field('tab_title_column_1');
	$tt2 = get_sub_field('tab_title_column_2');
	$desc1 = get_sub_field('description_column_1');
	$desc2 = get_sub_field('description_column_2');
	$sp = get_sub_field('single_post');
	$es = get_sub_field('enable_sidebar');
	$sc = get_sub_field('sidebar_content');
	// echo '<pre>';
	//     print_r( get_sub_field('single_post')  );
	// echo '</pre>';
	// die;
?>
<?php if($navmenu) : ?>
	<div id="sub_buttons1"> 
	    <?php 
	    echo $navmenu;    	
	    if($privatebg) : ?>
	    	<div class="sections_banner" style="background-image: url(<?php echo $privatebg['url']; ?>);"></div>	    	
	    	<?php 
	    endif; ?>	    
	</div>
	<div class="clearfix"></div>
<?php endif; ?>
<?php 
	if($choose_content == 1) : ?>
		<div class='trending-content-list'>
			<div class="tabs">
			    <ul class="tab-links">
			        <li class="active"><a href="#tab1"><?php echo $tt1; ?></a></li>
			        <li><a href="#tab2"><?php echo $tt2; ?></a></li>
			        <li><a href="#tab3">Help</a></li>			        
			    </ul>
			    <div class="tab-border">
				    <div class="tab-content">
				        <div id="tab1" class="tab active">
				            <?php echo $desc1; ?>
				            <div class="clearfix"></div>
				            <?php 
				            	wp_reset_query();
				            	include 'content-trending-asx.php'; 
				            ?>
				        </div>
				 
				        <div id="tab2" class="tab">
				            <?php echo $desc2; ?>
				            <div class="clearfix"></div>
				            <?php 
					            wp_reset_query();
					            include 'content-trending-smlmed.php'; 
					        ?>
				        </div>
				 
				        <div id="tab3" class="tab">
				            test
				        </div>
				    </div>
				</div>
			</div>
		</div>
<?php
	else : 
		
		if($sp): 
			// override $post
			$post = $sp;
			setup_postdata( $post ); 

			?>
			<div class="story_leftcol">
				<h1><?php the_title(); ?></h1>	
			<?php
				//wp_reset_postdata();

				if( have_rows('module') ):				 	
					?>
					<div class=""> <?php
				    while( have_rows('module') ) : the_row();			    	
						if( get_row_layout() == 'reports' ):					
							include 'content-single.php';
						endif;						
					endwhile; ?>
					</div> <?php
				endif; ?>
			</div><?php
			wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		<?php endif; ?>
<?php endif; ?>