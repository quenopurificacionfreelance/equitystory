<?php
if( get_row_layout() == 'thumbnail' ):
	if( have_rows('content_repeater') ):
		while ( have_rows('content_repeater') ) : the_row();
			$thumbnail_image = get_sub_field('thumbnail_image');
			$thumbnail_text = get_sub_field('thumbnail_text');			
			
			if($content_indention == 2) :
				echo '<div class="thumbnail-content">';
				echo '<div class="common col-md-3 col-sm-3 center col-md-offset-'.$content_indention.' col-sm-offset-1"><div class="img-block">';
				echo '<img src="'.$thumbnail_image.'" alt="Thumbnail image">';
				echo '</div></div><div class="common col-md-5 col-sm-6"><div class="thumbnailtext">'.$thumbnail_text.'</div><div class="border-bottom2 absolute-bottom"></div></div></div><div class="clearfix"></div>';
			else :
				echo '<div class="thumbnail-content">';
				echo '<div class="common col-md-3 col-sm-3 center col-md-offset-'.$content_indention.' col-sm-offset-1"><div class="img-block">';
				echo '<img src="'.$thumbnail_image.'" alt="Thumbnail image">';
				echo '</div></div><div class="common col-md-6 col-sm-6"><div class="thumbnailtext">'.$thumbnail_text.'</div><div class="border-bottom2 absolute-bottom"></div></div></div><div class="clearfix"></div>';
			endif;
		endwhile;
	endif;
endif;
?>