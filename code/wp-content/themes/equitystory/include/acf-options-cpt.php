<?php
//Options ACF
if(function_exists('acf_add_options_page')) {
	//acf_add_options_page();
	// acf_add_options_sub_page('Header');
	// acf_add_options_sub_page('Footer');

	acf_add_options_page(array(
		'page_title' => 'Theme Options',
		'menu_title' => 'Theme Options',
		'menu_slug' => 'theme-options',
		'capability' => 'edit_posts',
		'parent_slug' => '',
		'position' => false,
		'icon_url' => false,
		'redirect' => false
	));

	acf_add_options_page(array(
		'page_title' => 'Header',
		'menu_title' => 'Header',
		'menu_slug' => 'theme-options-header',
		'capability' => 'edit_posts',
		'parent_slug' => 'theme-options',
		'position' => false,
		'icon_url' => false,		
	));

	acf_add_options_page(array(
		'page_title' => 'Footer',
		'menu_title' => 'Footer',
		'menu_slug' => 'theme-options-footer',
		'capability' => 'edit_posts',
		'parent_slug' => 'theme-options',
		'position' => false,
		'icon_url' => false,		
	));

	acf_add_options_page(array(
		'page_title' => 'Featured Reports',
		'menu_title' => 'Featured Reports',
		'menu_slug' => 'theme-options-featured-reports',
		'capability' => 'edit_posts',
		'parent_slug' => 'theme-options',
		'position' => false,
		'icon_url' => false,		
	));

	acf_add_options_page(array(
		'page_title' => 'Post Settings',
		'menu_title' => 'Post Settings',
		'menu_slug' => 'post-settings',
		'capability' => 'edit_posts',
		'parent_slug' => 'edit.php',
		'position' => false,
		'icon_url' => false,
		'redirect' => false
	));
}
// End of options
?>