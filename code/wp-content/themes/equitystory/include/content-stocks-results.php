<div class="container mobile padding-mobile">
	<h1><?php the_title(); ?></h1>
</div>
<?php	
	$sr = get_sub_field('choose_categories');
	//echo $sr;	
	foreach ($sr as $rc) :
		$rt = $rc->term_id;										
		$result_cat = $rt.",". + $result_cat;
		
	endforeach;
	//echo $result_cat;

	$args = array(
		'posts_per_page' => 999,
		'post_status' => array( 'publish', 'private' ),
		'orderby' => date,
		'order' => DEC,
		'category__in' => array($result_cat)
	);
	query_posts($args);
	while (have_posts()) : the_post();	
		if( have_rows('stocks_repeater') ):
			while ( have_rows('stocks_repeater') ) : the_row();
				$stock_name = get_sub_field('stock_name');
				$date_bought = get_sub_field('date_bought');
				$dbp = get_sub_field('date_bought_price');
				$date_sold = get_sub_field('date_sold');
				$dsp = get_sub_field('date_sold_price');
				$profit = $dsp - $dbp; 
				$percent = ($profit / $dbp)*100;
				?>
				

			<div class="result-box desktop">	
				<div class="grid_2 stock-name">
				<?php echo $stock_name; ?>
				</div>
				<div class="grid_2 date-bought">
				<?php echo $date_bought; ?>
				</div>
				<div class="grid_1 dbp">
				<?php echo "$".$dbp; ?>
				</div>
				<div class="grid_2 date-sold">
				<?php echo $date_sold; ?>
				</div>
				<div class="grid_1 dsp">
				<?php echo "$".$dsp; ?>
				</div>
				<div class="grid_2 profit">
				<?php echo "$".round($profit, 2); ?>
				</div>
				<div class="grid_2 percent omega">
				<?php echo round($percent, 2)."%"; ?>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="result-box mobile">					
				<div class="grid_2 stock-name">
				<?php echo $stock_name; ?>
				</div>
				<div class="grid_2 date-bought">
				<?php echo "<strong>Date bought:</strong> ".$date_bought; ?>
				</div>
				<div class="grid_1 dbp">
				<?php echo "<strong>Price:</strong> $".$dbp; ?>
				</div>
				<div class="grid_2 date-sold">
				<?php echo "<strong>Sold:</strong> ".$date_sold; ?>
				</div>
				<div class="grid_1 dsp">
				<?php echo "<strong>Price:</strong> $".$dsp; ?>
				</div>
				<div class="grid_2 profit">
				<?php echo "<strong>Profit:</strong> $".round($profit, 2); ?>
				</div>
				<div class="grid_2 percent omega">
				<?php echo "<strong>Loss:</strong> ".round($percent, 2)."%"; ?>
				</div>
				<div class="clearfix"></div>
			</div>
				<?php
				
			endwhile;
		endif;
	endwhile;
?><div class="clearfix"></div>