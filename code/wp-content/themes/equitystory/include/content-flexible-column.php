<?php
	$enable_class = get_sub_field('enable_class');
	$class_right = get_sub_field('class_right');
	$class_left = get_sub_field('class_left');	

	// if(!empty($enable_class)) :
	// endif; 
	echo "<div class='container'><div class='grid_6 flexi-column-right omega'>";
	if( have_rows('content_left') ):
		while ( have_rows('content_left') ) : the_row();
			wp_reset_query();
			if( get_row_layout() == 'google_map' ):
				$map = get_sub_field('map');
				$loc = '<div class="acf-map">';
				$loc .= '<div class="marker" data-lat="'.$map['lat'].'" data-lng="'.$map['lng'].'">';
				$loc .= '</div></div>';
				echo $loc;
			endif;
			
				
			wp_reset_query();
			if( get_row_layout() == 'gravity_forms' ):
				$form = get_sub_field('form');
				echo $form;
			endif;
			
				
			wp_reset_query();
			if( get_row_layout() == 'content' ):
				$col_content = get_sub_field('col_content');
				echo $col_content;
			endif;
			
				
			wp_reset_query();
			if( get_row_layout() == 'repeater_content' ):					
				if( have_rows('content_repeater') ):
					while ( have_rows('content_repeater') ) : the_row();
						$image = get_sub_field('image');
						$content = get_sub_field('content');
						$link_image = get_sub_field('link_image');

						echo $image;
						echo $content;
						echo $link_image;
					endwhile;
				endif;
			endif;
			wp_reset_query();
		endwhile;
	endif;
	echo "</div>";
	echo "<div class='grid_6 flexi-column-right'>";
	if( have_rows('content_right') ):
		while ( have_rows('content_right') ) : the_row();
			wp_reset_query();
			if( get_row_layout() == 'google_map' ):
				$map2 = get_sub_field('map');
				//echo $map2;

				$loc1 = '<div class="acf-map">';
				$loc1 .= '<div class="marker" data-lat="'.$map2['lat'].'" data-lng="'.$map2['lng'].'">';
				$loc1 .= '</div></div>';
				echo $loc1;
			endif;			
				
			wp_reset_query();
			if( get_row_layout() == 'gravity_forms' ):
				$form2 = get_sub_field('form');				
				echo do_shortcode('[gravityform id="' . $form2['id'] . '" title="true" description="true" ajax="true"]');
			endif;
			
				
			wp_reset_query();
			if( get_row_layout() == 'content' ):
				$col_content2 = get_sub_field('col_content');
				echo $col_content2;
			endif;
			
				
			wp_reset_query();
			if( get_row_layout() == 'repeater_content' ):					
				if( have_rows('content_repeater') ):
					while ( have_rows('content_repeater') ) : the_row();
						$image2 = get_sub_field('image');
						$content2 = get_sub_field('content');
						$link_image2 = get_sub_field('link_image');

						echo $image2;
						echo $content2;
						echo $link_image2;
					endwhile;
				endif;
			endif;
			wp_reset_query();
		endwhile;
	endif;	
	echo "</div></div>";
	
?>