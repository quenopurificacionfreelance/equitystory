<?php
//content variables
	$the_story = get_sub_field('the_story');
	$why_buy = get_sub_field('why_buy');
	$buy_under = get_sub_field('buy_under');
	$year_high = get_sub_field('year_high');
	$year_low = get_sub_field('year_low');
	$upload_graph = get_sub_field('upload_graph');
	$f_year = get_sub_field('f_year');
	$s_year = get_sub_field('s_year');
	$recomendation = get_sub_field('recomendation');
	$status = get_sub_field('status');
	$market_cap = get_sub_field('market_cap');
	$eps_c = get_sub_field('eps_c');
	$eps_c_2 = get_sub_field('eps_c_2');
	$eps_c_3 = get_sub_field('eps_c_3');
	$pe_ratio_x = get_sub_field('pe_ratio_x');
	$pe_ratio_x_2 = get_sub_field('pe_ratio_x_2');
	$pe_ratio_x_3 = get_sub_field('pe_ratio_x_3');
	$dps_c = get_sub_field('dps_c');
	$dps_c_2 = get_sub_field('dps_c_2');
	$dps_c_3 = get_sub_field('dps_c_3');
	$dividend_yield = get_sub_field('dividend_yield');
	$dividend_yield_2 = get_sub_field('dividend_yield_2');
	$dividend_yield_3 = get_sub_field('dividend_yield_3');
	$analysis = get_sub_field('analysis');
	$current_sentiment = get_sub_field('current_sentiment'); ?>

	<div class='grid_12 omega'>
		<div class='row'> 
			<div class='box1_story'>
				<h2><span>THE STORY</span></h2>
			</div>
			<div class='box1_story bg_box1'>
				<div class=''>
					<div class=''>
						<?php echo $the_story; ?>
						<h3>WHY BUY</h3>
						<?php echo $why_buy; ?>
						<div class='bg_box8'>
							<div>
								<div>Buy<br>UNDER</div>
								<div>
									<?php 
										if($buy_under != 0) :
											echo "$".number_format($buy_under, 2); 
										else :
											echo "$0.00"; 
										endif;
									?>
								</div>
								<div>
									<img id='ctl00_placeholderBody_ctl59_imgBuyYN' src='/images/icon_buy.png'>
								</div>
							</div>                 								                   
							<div>
								<div class='white_txt'>YR HIGH</div>
								<div class='brown_txt3'>
									<?php 
										if($year_high != 0) :
											echo "$".number_format($year_high, 2); 
										else :
											echo "$0.00"; 
										endif;
									?>
								</div>
								<div class='white_txt'>YR LOW</div>
								<div class='brown_txt3'>
									<?php 
										if($year_low != 0) :
											echo "$".number_format($year_low, 2); 
										else :
											echo "$0.00"; 
										endif;
									?>
								</div>
								<div class='clearfix'></div>
							</div>
						</div>
					</div>
				</div>
				<div class='clearfix'></div>
			</div>
			<div class='borderbottom'></div>

			<div class='box2_story'>
				<h2><span>THE DETAILS</span></h2>
			</div>
			<div class='box2_story bg_box2'>
				<div class='grid_12 omega'>
					<div class='grid_6'>
						<div class="header-title">
							<div class="grid_4">
								Forecast
							</div>
							<div class="grid_4">
								Current
							</div>
							<div class="grid_2">
								<?php echo $f_year; ?>
							</div>
							<div class="grid_2 omega">
								<?php echo $s_year; ?>
							</div>
						</div>
						<div class="content-dt">
							<div class="grid_4">
								Market Cap
							</div>
							<div class="grid_4">
								<?php echo "$".number_format($market_cap, 2); ?>
							</div>
							<div class="grid_2">
								&nbsp;
							</div>
							<div class="grid_2 omega">
								&nbsp;
							</div>
						</div>
						<div class="content-dt">
							<div class="grid_4">
								EPS (c)
							</div>
							<div class="grid_4">
								<?php 
									if($eps_c != 0) :
										echo "$".number_format($eps_c, 2); 
									else :
										echo "-"; 
									endif;
								?>
							</div>
							<div class="grid_2">
								<?php 
									if($eps_c_2 != 0) :
										echo "$".number_format($eps_c_2, 2); 
									else :
										echo "-"; 
									endif;
								?>
							</div>
							<div class="grid_2 omega">
								<?php 
									if($eps_c_3 != 0) :
										echo "$".number_format($eps_c_3, 2); 
									else :
										echo "-"; 
									endif;
								?>
							</div>
						</div>
						<div class="content-dt">
							<div class="grid_4">
								PE Ratio (x)
							</div>
							<div class="grid_4">
								<?php 
									if($pe_ratio_x != 0) :
										echo "$".number_format($pe_ratio_x, 2); 
									else :
										echo "-"; 
									endif;
								?>
							</div>
							<div class="grid_2">
								<?php 
									if($pe_ratio_x_2 != 0) :
										echo "$".number_format($pe_ratio_x_2, 2); 
									else :
										echo "-"; 
									endif;
								?>
							</div>
							<div class="grid_2 omega">
								<?php 
									if($pe_ratio_x_3 != 0) :
										echo "$".number_format($pe_ratio_x_3, 2); 
									else :
										echo "-"; 
									endif;
								?>
							</div>
						</div>						
					</div>
					<div class='grid_6 omega'>
						<div class="header-title">
							<div class="grid_4">
								Forecast
							</div>
							<div class="grid_4">
								Current
							</div>
							<div class="grid_2">
								<?php echo $f_year; ?>
							</div>
							<div class="grid_2 omega">
								<?php echo $s_year; ?>
							</div>
						</div>
						<div class="content-dt">
							<div class="grid_4">
								DPS (c)
							</div>
							<div class="grid_4">
								<?php 
									if($dps_c != 0) :
										echo "$".number_format($dps_c, 2); 
									else :
										echo "-"; 
									endif;
								?>
							</div>
							<div class="grid_2">
								<?php 
									if($dps_c_2 != 0) :
										echo "$".number_format($dps_c_2, 2); 
									else :
										echo "-"; 
									endif;
								?>
							</div>
							<div class="grid_2 omega">
								<?php 
									if($dps_c_3 != 0) :
										echo "$".number_format($dps_c_3, 2); 
									else :
										echo "-"; 
									endif;
								?>
							</div>
						</div>
						<div class="content-dt">
							<div class="grid_4">
								Dividend Yield (%)
							</div>
							<div class="grid_4">
								<?php 
									if($dividend_yield != 0) :
										echo "$".number_format($dividend_yield, 2); 
									else :
										echo "-"; 
									endif;
								?>
							</div>
							<div class="grid_2">
								<?php 
									if($dividend_yield_2 != 0) :
										echo "$".number_format($dividend_yield_2, 2); 
									else :
										echo "-"; 
									endif;
								?>
							</div>
							<div class="grid_2 omega">
								<?php 
									if($dividend_yield_3 != 0) :
										echo "$".number_format($dividend_yield_3, 2); 
									else :
										echo "-"; 
									endif;
								?>
							</div>
						</div>
					</div>
				</div>
				<div class='clearfix'></div>
			</div>
			<div class='borderbottom2'></div>

			<div class="grid_6">
				<div class='box2_story '>
					<h2><span>ANALYSIS</span></h2>
				</div>			
				<div class='box2_story bg_box1 bottm'>
					<div class=''>
						<?php echo $analysis; ?>
					</div>
				</div>
				<div class='borderbottom last'></div>
			</div>
			<div class="grid_6 omega">
				<div class='box2_story '>
					<h2><span>CURRENT SENTIMENT</span></h2>
				</div>			
				<div class='box2_story bg_box1 bottm'>
					<div class=''>
						<?php echo $current_sentiment; ?>
					</div>
				</div>
				<div class='borderbottom last'></div>
			</div>
		</div>		
	</div>


	<?php


	
	// echo $upload_graph;
	// echo $recomendation;
	// echo $status;
	// echo $market_cap;
	// echo $eps_c;
	// echo $eps_c_2;
	// echo $eps_c_3;
	// echo $pe_ratio_x;
	// echo $pe_ratio_x_2;
	// echo $pe_ratio_x_3;
	// echo $dps_c;
	// echo $dps_c_2;
	// echo $dps_c_3;
	// echo $dividend_yield;
	// echo $dividend_yield_2;
	// echo $dividend_yield_3;
	// echo $analysis;
	// echo $current_sentiment;
?>	