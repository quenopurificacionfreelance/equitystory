<?php

add_action('init', 'solutions_register');

//Custom Post Type for Panel Thumbnails
function solutions_register() {
	$labels = array(
		'name' => _x('Solutions Pathways CPT', 'post type general name'),
		'singular_name' => _x('Solutions', 'post type singular name'),
		'add_new' => _x('Add new content', 'Content item'),
		'add_new_item' => __('Add content'),
		'edit_item' => __('Edit content'),
		'new_item' => __('New content'),
		'view_item' => __('View content'),
		'search_items' => __('Search content'),
		'not_found' => __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title'),
		'rewrite' => true
	);

	register_post_type('solutions', $args );
}