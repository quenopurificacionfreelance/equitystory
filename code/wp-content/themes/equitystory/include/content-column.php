<?php 

	$column_1_title = get_sub_field('column_1_title');
	$column_2_title = get_sub_field('column_2_title');
	
	$colcontent = "<div class='clearfix'></div><div class='column-content datatable'>";
	$colcontent .= "<div class='grid_6 h'><h3>".$column_1_title."</h3>";

	if( have_rows('column_content') ):
		while ( have_rows('column_content') ) : the_row();
			$column_1_text = get_sub_field('column_1_text');
			//$column_2_text = get_sub_field('column_2_text');						
			$colcontent .= "<div class='border-r-b'><div class='box'>".$column_1_text."</div></div>";
			//$colcontent .= "<div class='grid_6 omega border-r-b'><div class='box'>".$column_2_text."</div></div>";
			$colcontent .= "<div class='clearfix'></div>";		
		endwhile;
	endif;
	$colcontent .= "</div>";
	$colcontent .= "<div class='grid_6 h omega'><h3>".$column_2_title."</h3>";
	if( have_rows('column_content') ):
		while ( have_rows('column_content') ) : the_row();
			//$column_1_text = get_sub_field('column_1_text');
			$column_2_text = get_sub_field('column_2_text');						
			//$colcontent .= "<div class='grid_6 border-r-b'><div class='box'>".$column_1_text."</div></div>";
			$colcontent .= "<div class='border-r-b'><div class='box'>".$column_2_text."</div></div>";
			$colcontent .= "<div class='clearfix'></div>";		
		endwhile;
	endif;
	$colcontent .= "</div><div class='clearfix'></div>";
	
	

	

	$colcontent .= "</div><div class='clearfix'></div>";
	echo $colcontent;

?>
