<?php
//content variables
	$aes_content = get_sub_field('aes_content');
	$v_embed_video = get_sub_field('v_embed_video');
	$v_name = get_sub_field('v_name');
	$v_title = get_sub_field('v_title');
	$v_video_description = get_sub_field('v_video_description');
	$ar_result_percentage = get_sub_field('ar_result_percentage');
	$ar_result_summary = get_sub_field('ar_result_summary');
	$ar_results_button = get_sub_field('ar_results_button');
	$ar_results_additional_text = get_sub_field('ar_results_additional_text');
	$s_choose_form = get_sub_field('s_choose_form'); 
	$form_description = get_sub_field('form_description'); 

	$hero_background = get_sub_field('hero_background'); 
	$hero_title = get_sub_field('hero_title'); 
	$hero_content = get_sub_field('hero_content'); 
	$hero_video = get_sub_field('hero_video'); 
	$pop_up_form = get_sub_field('pop_up_form');
	$pop_button_text = get_sub_field('pop_button_text'); ?>
	

	
		<div class="mobile-hidden">
			<div id="home_main">
				<div class="desc">
					<?php echo $aes_content; ?>
				</div> <!-- end desc -->

				<div id="homepage-result-panel">
					<p id="result-percentage"><?php echo $ar_result_percentage; ?><span>%</span></p>
					<div id="result-summary"><?php echo $ar_result_summary; ?></div>
					<p id="results-button" onclick="window.location=&quot;/testimonials&quot;"><a href="/testimonials"><?php echo $ar_results_button; ?></a></p>
					<p id="results-past-results"><?php echo $ar_results_additional_text; ?></p>
				</div>

				<div class="flash">
					<p><a href="#" class="<?php echo $v_embed_video; ?>" id="YSVideo"><img src="https://www.equitystory.com.au/images/david-trailer.png" alt="See how it works" width="188" height="109" border="0"></a></p>
					<p id="david"><?php echo $v_name; ?></p>
					<p id="david-title"><?php echo $v_title; ?></p>
					<p id="david-video-intro"><?php echo $v_video_description; ?></p>
				</div> <!-- end flash -->

				<div class="subscribe">
					<div class="left_desc">
						<div id="trial-box">
							<?php 			
								echo $form_description;				    
							    gravity_form_enqueue_scripts($s_choose_form['id'], true);
							    gravity_form($s_choose_form['id'], false, true, false, '', true, 1); 
							?>
						</div>
					</div>
					<div class="clearfix"></div>	
				</div>

			</div>
			<div class="clearfix"></div>
		</div>
		<div class="mobile">
			<div class="mobile-hero" style="background: url(<?php echo $hero_background['url'] ?>);">
				<div class="hero-text">
					<h2><?php echo $hero_title; ?></h2>
					<?php echo $hero_content; ?>
					<?php if($hero_video) : ?>
						<button class="<?php echo $hero_video; ?>">Play video</button>				
					<?php endif; ?>
				</div>
			</div>
			<?php if($pop_button_text) : ?>
			<div class="trial-form">
				<p><?php echo $pop_button_text; ?></p>
				<?php if($pop_up_form) : ?>
					<a href="#" class="<?php echo $pop_up_form; ?>">Try for free</a>
				<?php endif; ?>
			</div>
			<?php endif; ?>
		</div>