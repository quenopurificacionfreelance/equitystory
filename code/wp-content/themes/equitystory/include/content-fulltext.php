<?php
if( get_row_layout() == 'full_text' ):
	$fulltext = get_sub_field('content');
	echo "<div class='infoText'>".$fulltext."</div>";
endif;
?>
