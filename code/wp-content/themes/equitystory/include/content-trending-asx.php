<div class="asx">
	<div class="header-tables">
		<div class="grid_3">
			STOCK
		</div>
		<div class="grid_2 ">
		WEEKLY TREND
		</div>
		<div class="grid_2 ">
		DAILY TREND
		</div>
		<div class="grid_2 ">
		EQUITY STORY INDICATOR
		</div>
		<div class="grid_3 omega">
		RECOMMENDATION
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="content-tables">
	<?php 
		$args = array(
			'posts_per_page' => 999,
			'post_status' => array( 'publish', 'private' ),
			'orderby' => date,
			'order' => DEC,
			'category__in' => array(10)
		);
		query_posts($args);
		while (have_posts()) : the_post();	
			if( have_rows('module') ):				 	
			    while( have_rows('module') ) : the_row();			    	
					if( get_row_layout() == 'trending_only_report' ):					
						$upload_trending_chart = get_sub_field('upload_trending_chart');
						$weekly_trend = get_sub_field('weekly_trend');
						$daily_trend = get_sub_field('daily_trend');
						$es_indicator = get_sub_field('es_indicator');
						$trending_status = get_sub_field('trending_status');
						$trending_recommendation = get_sub_field('trending_recommendation');
						$display_on_top_10_trending_stocks = get_sub_field('display_on_top_10_trending_stocks');
						$display_on_asx_200_stocks = get_sub_field('display_on_asx_200_stocks');
						$what_position_on_top_10 = get_sub_field('what_position_on_top_10');
						$trending_further_information = get_sub_field('trending_further_information');


						$weekly_trend = strtolower($weekly_trend);
						$daily_trend = strtolower($daily_trend);
						$es_indicator = strtolower($es_indicator);
						// echo $upload_trending_chart."<br/>";
						// echo $weekly_trend."<br/>";
						// echo $daily_trend."<br/>";
						// echo $es_indicator."<br/>";
						// echo $trending_status."<br/>";
						// echo $trending_recommendation."<br/>";
						 // echo $display_on_top_10_trending_stocks."<br/>";
						 // echo $display_on_asx_200_stocks."<br/>";
						// echo $what_position_on_top_10."<br/>";
						// echo $trending_further_information."<br/>";
						if($display_on_asx_200_stocks == 'Yes') :
							if($weekly_trend == 'yes' &&  $daily_trend == 'yes' && $es_indicator == 'yes') :
								$stat = 'active';
							elseif($weekly_trend == 'no' &&  $daily_trend == 'no' && $es_indicator == 'no') :
								$stat = 'none';
							else :
								$stat = 'not-active';
							endif;
							?>	
								<div class="status <?php echo $stat; ?>">
									<div class="grid_3">
										<?php the_title(); ?>
									</div>
									<div class="grid_2 ">
										<?php echo "<span class='".$weekly_trend."'></span>"; ?>
									</div>
									<div class="grid_2 ">
										<?php echo "<span class='".$daily_trend."'></span>"; ?>
									</div>
									<div class="grid_2 ">
										<?php echo "<span class='".$es_indicator."'></span>"; ?>
									</div>
									<div class="grid_3 omega">
										<?php echo $trending_recommendation; ?>
									</div>
									<div class="clearfix"></div>
								</div>
							<?php
						endif;
					endif;						
				endwhile;
			endif;
		endwhile;
	?>
	</div>
</div>