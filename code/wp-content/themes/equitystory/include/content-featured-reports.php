<?php
	$header_text = get_field('header_text', 'option');
	$ltext = get_field('link_text', 'option');	
?>

<div id="featured-reports-content">
	<div class="sections">
		<div class="reports">
			<div class="desc">
				<p align="center"><?php echo $header_text; ?></p>
			</div> <!-- end reports -->

			<?php

			while(has_sub_field('featured_repeater', 'option')):
				$featured_class = get_sub_field('featured_class');
				$details = get_sub_field('details');
				$page_link_registered = get_sub_field('page_link_if_user_is_registered');
				$page_link_not_registered = get_sub_field('page_link_if_user_is_not_registered');				
				$imagefeatured = get_sub_field('background_image'); 
				$mobile_active_icon = get_sub_field('mobile_active_icon');				
				$cats = get_sub_field('total_recommendation');
				?>
				<div class="featured-column <?php echo $featured_class; ?>">
					<div class="top" style="background-image: url(<?php echo $imagefeatured; ?>);">
						<?php
							if (is_user_logged_in()) {
								echo '<p class="header"><a class="icon-'.$featured_class.'" style="background-image: url('.$mobile_active_icon["url"].');" href="'.$page_link_registered.'">&nbsp;</a></p>';
							} else {
								echo '<p class="header"><a class="icon-'.$featured_class.'" style="background-image: url('.$mobile_active_icon["url"].');" href="'.$page_link_not_registered.'">&nbsp;</a></p>';
							}
						?>						
						<div class="desc custom"><?php echo $details; ?></div>
						<?php
						//Check if user is logged-in
							if (is_user_logged_in()) {
								echo '<p class="link"><a href="'.$page_link_registered.'">'.$ltext.'</a></p>';
							} else {
								echo '<p class="link"><a href="'.$page_link_not_registered.'">'.$ltext.'</a></p>';
							}
						?>
						
					</div> <!-- end top -->
					<div class="clearfix"></div>
					<div class="count">
						<div class="trending_numbers">
						
							<?php
								foreach ($cats as $category) :
									$termid = $category->term_id;										
									$cat = $termid.",". + $cat;
								endforeach;

								$args = array(
									'posts_per_page' => 999,
									'post_status' => array( 'publish', 'private' ),
									'orderby' => date,
									'order' => DEC,
									'category__in' => array($cat)
								);
								query_posts($args);
								$totalfeatured = 1;
								$x = 0;
								while (have_posts()) : the_post();
									
									$ftotal = $totalfeatured+$x;							
									$x++;
								endwhile;
								
								if($ftotal!='') :
									echo $ftotal;
								else :
									echo '0';
								endif;
								$ftotal = 0;
							?>
						</div>
						<p class="fleft">current<br> recommendations</p>
						<div class="clearfix"></div>
					</div>
				</div>  <!-- end trending -->				
			<?php  endwhile; ?>						
		</div> <!-- end sections -->
	</div> <div class="clearfix"></div>
</div>