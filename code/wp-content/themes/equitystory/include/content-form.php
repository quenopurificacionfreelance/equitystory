<?php 
if( get_row_layout() == 'form' ):
	$form_object = get_sub_field('gravity_form');	
	$hssform = '<div class="form-hss">';
	if($content_indention == 2) :
		$hssform .= '<div class="col-md-8 col-sm-8 col-md-offset-'.$content_indention.' col-sm-offset-'.$content_indention.'"><div class="form-box">';
	else :	
		$hssform .= '<div class="col-md-10 col-md-10 col-md-offset-'.$content_indention.' col-sm-offset-'.$content_indention.'"><div class="form-box">';
	endif;		
	$hssform .= do_shortcode('[gravityform id="' . $form_object['id'] . '" title="true" description="true" ajax="true"]');	
	$hssform .= '</div></div><div class="clearfix"></div></div>';
	echo $hssform;
endif;
?>
