<?php
if( get_row_layout() == 'featured' ):
	$featured_text_1 = get_sub_field('featured_text_1');
	$featured_text_2 = get_sub_field('featured_text_2');

	echo '<div class="featured"><div class="col-md-6 col-sm-6 featured-color cnt1">';
	echo '<div class="col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1 cta">';
	echo $featured_text_1;
	echo '</div></div>';

	echo '<div class="col-md-6 col-sm-6 featured-color cnt2">';
	echo '<div class="col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1 cta">';
	echo $featured_text_2;
	echo '</div></div></div>';
	echo '<div class="clearfix"></div>';

endif;
?>