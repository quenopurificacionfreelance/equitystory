<?php

add_action('init', 'entity_register');

//Custom Post Type for Panel Thumbnails
function entity_register() {
	$labels = array(
		'name' => _x('Homepage Entity CPT', 'post type general name'),
		'singular_name' => _x('Entity', 'post type singular name'),
		'add_new' => _x('Add new entity', 'journey item'),
		'add_new_item' => __('Add entity'),
		'edit_item' => __('Edit entity'),
		'new_item' => __('New entity'),
		'view_item' => __('View entity'),
		'search_items' => __('Search entity'),
		'not_found' => __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title'),
		'rewrite' => true
	);

	register_post_type('entity', $args );
}