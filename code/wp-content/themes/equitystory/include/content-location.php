<?php
if( get_row_layout() == 'location' ):
	if( have_rows('add_location') ):
		while ( have_rows('add_location') ) : the_row();
			$header_title = get_sub_field('header_title');
			$location_text = get_sub_field('location_text');
			$location_map = get_sub_field('location_map');
			if( !empty($location_map) ):
				$googlelink = urlencode($location_map['address']);
			endif;
			if($content_indention == 2) :
				$loc1 = '<div class="location">';
				$loc1 .= '<div class="col-md-4 col-sm-4 col-md-offset-'.$content_indention.' col-sm-offset-'.$content_indention.'">';
				if($header_title) :
					$loc1 .= '<div class="cta"><p>'.$header_title.'</p></div>';
				endif;
				$loc1 .= $location_text;
				if( !empty($location_map) ):
					$loc1 .= '<p><a href="http://www.google.com/maps/preview#!q='.$googlelink.'" target="_blank">View Map</a></p>';
				endif;
				$loc1 .= '</div>';
				if( !empty($location_map) ) :
					$loc1 .= '<div class="col-md-3 col-sm-3 col-md-offset-1 col-sm-offset-1">';
					$loc1 .= '<div class="acf-map">';
					$loc1 .= '<div class="marker" data-lat="'.$location_map['lat'].'" data-lng="'.$location_map['lng'].'">';
					$loc1 .= '<p class="address">'.$location_map['address'].'</p>';
					$loc1 .= '</div>';
					$loc1 .= '</div>';
				endif;
				$loc1 .= '</div>';
				$loc1 .= '<div class="clearfix"></div>';
				echo $loc1;
				
			else :
				$loc1 = '<div class="location">';
				$loc1 .= '<div class="col-md-5 col-sm-5 col-md-offset-'.$content_indention.' col-sm-offset-'.$content_indention.'">';
				if($header_title) :
					$loc1 .= '<div class="cta"><p>'.$header_title.'</p></div>';
				endif;
				$loc1 .= $location_text;
				$loc1 .= '</div>';
				if( !empty($location_map) ) :
					$loc1 .= '<div class="col-md-4 col-sm-4 col-md-offset-1 col-sm-offset-1">';
					$loc1 .= '<div class="acf-map">';
					$loc1 .= '<div class="marker" data-lat="'.$location_map['lat'].'" data-lng="'.$location_map['lng'].'">';
					$loc1 .= '<p class="address">'.$location_map['address'].'</p>';
					$loc1 .= '</div>';
					$loc1 .= '</div>';
				endif;
				$loc1 .= '</div>';
				$loc1 .= '<div class="clearfix"></div>';
				echo $loc1;
				
			endif;
		endwhile;
	endif;
endif;
?>