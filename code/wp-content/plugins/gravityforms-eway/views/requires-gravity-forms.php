
<div class="error">
	<p><?php printf(__('Gravity Forms eWAY requires <a target="_blank" href="%s">Gravity Forms</a> to be installed and activated.', 'gravityforms-eway'),
		'http://webaware.com.au/get-gravity-forms'); ?></p>
</div>
