<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'equitystory');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', 'P@ssw0rd');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',G4~m,`;jjrl:b8TfIlX/Vavtgt_*N!/F|??I:nJ+=xVVk1j=o5wyk+MaSuKuA?`');
define('SECURE_AUTH_KEY',  'XP(8G+]M+WbNa:JNMZcw GV%z[9-s}|G+A&_M;T_KB:.v<r5v>3%K_t]i>qq:4L9');
define('LOGGED_IN_KEY',    'RXx!%5Po=vHq76Uuv.JUb:K]GQ2s W24VH~[m|qft2Y]HH|~b$+gz$P-x]zQm|Ki');
define('NONCE_KEY',        '_<zk#yqQ+<``Ve-5-KtECP]&!&.H)Htxb8p>M6HvO3#XoA8HVEHl$Wm)iU]2-|JV');
define('AUTH_SALT',        '%r;`?a|dq6`<rTj|}fM|jhqx)9RjI]Hu?<-Z-DKD#gUL5P2>wJUG6e*JChH4K=HH');
define('SECURE_AUTH_SALT', 'r~jD-X)8U@ao_[WT2R2.u~7|yu:+ScBr>lHtE5Z:1[Q!~1t5GG4EH<uGPr(<&fhN');
define('LOGGED_IN_SALT',   'E|#4w[mQ$l_`z?v:UVBxJ)X|~;R-uZ@Z]WNo(JWA9M(s(s~A#.#7YxY+t|$n~:;a');
define('NONCE_SALT',       ':u  Q@B`GvpB IpU5cUq :G7QR5|eT2c}-OTa.U#ALvQ-n@[q1.L=+`_</6xLjN.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'es_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
