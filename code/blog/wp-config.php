<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'equityst_wp1');

/** MySQL database username */
define('DB_USER', 'equityst_wp1');

/** MySQL database password */
define('DB_PASSWORD', 'Z@2X7oCp[C07*#3');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'adDdWr7XWNFrSK1uXebHUh4mzFasRRWMYS7y7WPn2THDLgmYrkdHVAI7GDj2cLTP');
define('SECURE_AUTH_KEY',  '4QxSi6A0SMtPHSf98CHitZbTuiYR4iXxolG6ADSfKGNBZLsktiuRLHipjMVAkkuH');
define('LOGGED_IN_KEY',    'mPQw24OwW8BXeQ791G2JhGNRvnVDWuQxssU9gBKlpJuK1zZvw5y0vQXHEnjdeuda');
define('NONCE_KEY',        'H3PzrvjfC7i3gCXHTMh7Dqusc6vu3q9TvdwX6bElB0KYYcoCJEbOrRwRdVBrptYK');
define('AUTH_SALT',        'hJxmFKjdtSl5J7XGUZVBIvhRDU9SSibVETfAUvfDsvYNC58yxeuDA6mrCsfoCRru');
define('SECURE_AUTH_SALT', 'i0cHFwZKn2mQoqxLEjwd9QCfHOBoU1AYVPc1bqiGcTJSXlOeWTDYKfzTu1szOX4s');
define('LOGGED_IN_SALT',   'PBDYWTUshdAGMjV3eDH47uwZe1r7WX0fnsAstaN9m28duaTcznsfhWE6VILDayBw');
define('NONCE_SALT',       '6bNyHy2KptCgTfIv5uBm6IXXumgtF1jVGPpv93zHW9BpvbP8S28ElGnDKvA2ZoxA');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');